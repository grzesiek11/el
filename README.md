# El

![El logo](images/logo_128.webp)

**El** is a work-in-progress, general purpose, open source Discord bot.

It's made with Rust and uses [Serenity](https://github.com/serenity-rs/serenity) for communication with the Discord API. Formely it used Python and discord.py.

## Usage

You can find information on using El on [the wiki](https://gitlab.com/Grzesiek11/El/-/wikis/en/Home).

## The name

The name 'El' is short for 'eleven'. I wanted a name that's short enough for a prefix and I came up with that.

El is female because her name is similar to 'Chell', the Portal protagonist.

## License

El is available under [GNU AGPL 3.0](LICENSES/GNU_AGPL_3.0) or later. The El logo is available under [CC BY-SA 4.0](LICENSES/CC_BY-SA_4.0).

Also see [COPYING](COPYING).
