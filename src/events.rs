/*
 * This file is a part of the El project: <https://gitlab.com/Grzesiek11/El>
 * El is available under the terms of the GNU Affero General Public License, version 3 or later.
 * The license is distributed with the project, but if you don't have a copy, see <https://www.gnu.org/licenses/agpl-3.0.html> instead.
 */

use std::{collections::HashMap, sync::Arc};

use futures::future::BoxFuture;
use serenity::{async_trait, client::RawEventHandler, model::{channel::{Message, Reaction}, guild::Member}};
use mongodb::Database;

use crate::{features, Config};

#[derive(Clone)]
pub struct Context {
    pub c: serenity::client::Context,
    pub config: Arc<Config>,
    pub db: Database,
}

impl Context {
    fn new(discord_context: serenity::client::Context, config: Arc<Config>, db: Database) -> Self {
        Self {
            c: discord_context,
            config,
            db,
        }
    }
}

#[derive(Debug)]
pub enum Error {
    Discord(serenity::Error),
    Parser(cmdparse::Error),
    Custom(String),
}

impl From<serenity::Error> for Error {
    fn from(error: serenity::Error) -> Self {
        Self::Discord(error)
    }
}

impl From<cmdparse::Error> for Error {
    fn from(error: cmdparse::Error) -> Self {
        Self::Parser(error)
    }
}

impl From<String> for Error {
    fn from(error: String) -> Self {
        Self::Custom(error)
    }
}

impl From<&str> for Error {
    fn from(error: &str) -> Self {
        Self::Custom(error.to_string())
    }
}

pub type EventResult = Result<(), Error>;

pub struct Command {
    pub arguments: HashMap<String, cmdparse::ParsedArgument>,
    pub msg: Message,
}

type DiscordEvent = serenity::model::event::Event;

enum InputEvent {
    Discord(DiscordEvent),
}

pub enum Event {
    MessageCreate(Box<dyn Fn(Context, Message) -> BoxFuture<'static, EventResult> + Sync + Send>),
    ReactionAdd(Box<dyn Fn(Context, Reaction) -> BoxFuture<'static, EventResult> + Sync + Send>),
    ReactionRemove(Box<dyn Fn(Context, Reaction) -> BoxFuture<'static, EventResult> + Sync + Send>),
    GuildMemberAdd(Box<dyn Fn(Context, Member) -> BoxFuture<'static, EventResult> + Sync + Send>),
}

pub struct CommandMetadata {
    pub callback: Option<Box<dyn Fn(Context, Command) -> BoxFuture<'static, EventResult> + Sync + Send>>,
}

pub struct Plugin {
    pub events: Vec<Event>,
    pub commands: HashMap<String, cmdparse::Node>,
}

pub struct Handler {
    events: Vec<Event>,
    commands: cmdparse::Input,
    config: Arc<Config>,
    db: Database,
}

impl Handler {
    pub fn new(config: Config, db: Database) -> Self {
        let mut handler = Self {
            events: Vec::new(),
            commands: cmdparse::Input::Tree(HashMap::new()),
            config: Arc::new(config),
            db,
        };

        let plugins = vec![
            features::misc::register(),
            features::poll::register(),
            features::text::register(),
            features::info::register(),
            features::random::register(),
            features::roles::register(),
            features::activities::register(),
        ];

        for mut plugin in plugins {
            handler.events.append(&mut plugin.events);
            handler.add_commands(plugin.commands);
        }

        handler
    }

    async fn handle_event(&self, input_event: InputEvent, discord_context: serenity::client::Context) {
        let mut event_results = Vec::new();
        let mut error_channel = None;

        let context = Context::new(discord_context, self.config.clone(), self.db.clone());

        match input_event {
            InputEvent::Discord(discord_event) => match discord_event {
                DiscordEvent::MessageCreate(message_create_event) => {
                    let message = message_create_event.message;

                    for callback in self.events.iter().filter_map(
                        |event| if let Event::MessageCreate(c) = event { Some(c) } else { None }
                    ) {
                        event_results.push(callback(context.clone(), message.clone()).await);
                    }

                    match cmdparse::parse_string(&message.content, &self.commands, &["el!".to_string()]) {
                        Ok(Some((command, arguments, _, _))) => {
                            if let Some(metadata) = command.metadata.downcast_ref::<CommandMetadata>() {
                                if let Some(callback) = &metadata.callback {
                                    event_results.push(callback(context.clone(), Command { msg: message.clone(), arguments }).await);
                                }
                            }
                        },
                        Ok(None) => (),
                        Err(error) => event_results.push(Err(error.into())),
                    }

                    error_channel = Some(message.channel_id);
                },
                DiscordEvent::ReactionAdd(reaction_add_event) => {
                    let reaction = reaction_add_event.reaction;

                    for callback in self.events.iter().filter_map(
                        |event| if let Event::ReactionAdd(a) = event { Some(a) } else { None }
                    ) {
                        event_results.push(callback(context.clone(), reaction.clone()).await);
                    }
                },
                DiscordEvent::ReactionRemove(reaction_remove_event) => {
                    let reaction = reaction_remove_event.reaction;

                    for callback in self.events.iter().filter_map(
                        |event| if let Event::ReactionRemove(a) = event { Some(a) } else { None }
                    ) {
                        event_results.push(callback(context.clone(), reaction.clone()).await);
                    }
                },
                DiscordEvent::GuildMemberAdd(guild_member_add_event) => {
                    let member = guild_member_add_event.member;

                    for callback in self.events.iter().filter_map(
                        |event| if let Event::GuildMemberAdd(a) = event { Some(a) } else { None }
                    ) {
                        event_results.push(callback(context.clone(), member.clone()).await);
                    }
                },
                _ => (),
            },
        }

        for result in event_results {
            match result {
                Ok(()) => (),
                Err(error) => {
                    let error_message = format!("[ERROR] {:?}", error);

                    println!("{}", error_message);
                    if let Some(channel) = error_channel {
                        let _ = channel.say(&context.c.http, error_message).await;
                    }
                }
            }
        }
    }

    fn add_commands(&mut self, commands: HashMap<String, cmdparse::Node>) {
        if let cmdparse::Input::Tree(command_tree) = &mut self.commands {
            command_tree.extend(commands.into_iter());
        }
    }
}

#[async_trait]
impl RawEventHandler for Handler {
    async fn raw_event(&self, discord_context: serenity::client::Context, discord_event: DiscordEvent) {
        self.handle_event(InputEvent::Discord(discord_event), discord_context).await;
    }
}
