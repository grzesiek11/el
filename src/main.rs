/*
 * This file is a part of the El project: <https://gitlab.com/Grzesiek11/El>
 * El is available under the terms of the GNU Affero General Public License, version 3 or later.
 * The license is distributed with the project, but if you don't have a copy, see <https://www.gnu.org/licenses/agpl-3.0.html> instead.
 */

mod features;
mod utils;
mod events;
mod config;

use std::{fs::File, panic, process::exit};
use serenity::model::gateway::GatewayIntents;

use events::Handler;
use config::Config;

#[tokio::main]
async fn main() {
    let default_panic = panic::take_hook();
    panic::set_hook(Box::new(move |info| {
        default_panic(info);
        exit(1);
    }));

    let config: Config = serde_json::from_reader(
        File::open("config.json").unwrap()
    ).unwrap();

    let mongo = mongodb::Client::with_uri_str(&config.mongodb_uri).await.unwrap();
    let db = mongo.database(&config.database);

    let mut client = serenity::Client::builder(&config.discord_token, GatewayIntents::all())
        .raw_event_handler(Handler::new(config, db))
        .await.unwrap();

    client.start().await.unwrap();
}
