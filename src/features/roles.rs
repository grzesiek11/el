/*
 * This file is a part of the El project: <https://gitlab.com/Grzesiek11/El>
 * El is available under the terms of the GNU Affero General Public License, version 3 or later.
 * The license is distributed with the project, but if you don't have a copy, see <https://www.gnu.org/licenses/agpl-3.0.html> instead.
 */

use std::collections::HashMap;

use serenity::model::{channel::Reaction, guild::Member};

use crate::events::{Context, EventResult, Event, Plugin};

pub async fn reaction_add(ctx: Context, reaction: Reaction) -> EventResult {
    if let Some(roles) = ctx.config.plugins["roles"].try_as_map().unwrap()["reaction_roles"].try_as_map().unwrap().get(&reaction.message_id.to_string()) {
        if let Some(role) = roles.try_as_map().unwrap().get(&reaction.emoji.to_string()) {
            reaction.guild_id.unwrap()
                .member(&ctx.c.http, reaction.user_id.unwrap()).await?
                .add_role(&ctx.c.http, *role.try_as_u64().unwrap()).await?;
        }
    }

    Ok(())
}

pub async fn reaction_remove(ctx: Context, reaction: Reaction) -> EventResult {
    if let Some(roles) = ctx.config.plugins["roles"].try_as_map().unwrap()["reaction_roles"].try_as_map().unwrap().get(&reaction.message_id.to_string()) {
        if let Some(role) = roles.try_as_map().unwrap().get(&reaction.emoji.to_string()) {
            reaction.guild_id.unwrap()
                .member(&ctx.c.http, reaction.user_id.unwrap()).await?
                .remove_role(&ctx.c.http, *role.try_as_u64().unwrap()).await?;
        }
    }

    Ok(())
}

pub async fn guild_member_addition(ctx: Context, mut member: Member) -> EventResult {
    if let Some(role) = ctx.config.plugins["roles"].try_as_map().unwrap()["join_roles"].try_as_map().unwrap().get(&member.guild_id.to_string()) {
        member.add_role(&ctx.c.http, *role.try_as_u64().unwrap()).await?;
    }

    Ok(())
}

pub fn register() -> Plugin {
    Plugin {
        events: vec![
            Event::ReactionAdd(Box::new(move |context, reaction| Box::pin(reaction_add(context, reaction)))),
            Event::ReactionRemove(Box::new(move |context, reaction| Box::pin(reaction_remove(context, reaction)))),
            Event::GuildMemberAdd(Box::new(move |context, member| Box::pin(guild_member_addition(context, member)))),
        ],
        commands: HashMap::new(),
    }
}
