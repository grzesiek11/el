/*
 * This file is a part of the El project: <https://gitlab.com/Grzesiek11/El>
 * El is available under the terms of the GNU Affero General Public License, version 3 or later.
 * The license is distributed with the project, but if you don't have a copy, see <https://www.gnu.org/licenses/agpl-3.0.html> instead.
 */

use std::collections::HashMap;

use serenity::model::id::{ApplicationId, ChannelId};

use crate::events::{Context, Command, EventResult, Plugin, CommandMetadata};

async fn activity(ctx: Context, cmd: Command) -> EventResult {
    let channel = ChannelId(cmd.arguments["channel"].value().unwrap().as_u64().unwrap());
    let invite = channel.create_invite(&ctx.c.http, |i| i
        .target_type(serenity::model::invite::InviteTargetType::EmmbeddedApplication)
        .target_application_id(
            ApplicationId(cmd.arguments["activity"].value().unwrap().as_u64().unwrap())
        )
    ).await?;
    cmd.msg.channel_id.say(&ctx.c.http, invite.url()).await?;

    Ok(())
}

pub fn register() -> Plugin {
    Plugin {
        events: Vec::new(),
        commands: HashMap::from([
            ("activity".to_string(), cmdparse::Node::Command(cmdparse::Command {
                arguments: HashMap::from([
                    ("channel".to_string(), cmdparse::Argument {
                        kind: cmdparse::ArgumentKind::Positional(0),
                        value: cmdparse::Value::U64,
                        default: None,
                        repeating: cmdparse::Repeat::False,
                        required: true,
                    }),
                    ("activity".to_string(), cmdparse::Argument {
                        kind: cmdparse::ArgumentKind::Positional(1),
                        value: cmdparse::Value::U64,
                        default: None,
                        repeating: cmdparse::Repeat::False,
                        required: true,
                    }),
                ]),
                subcommands: HashMap::new(),
                parse_negative_numbers: false,
                metadata: Box::new(CommandMetadata {
                    callback: Some(Box::new(move |context, command| Box::pin(activity(context, command)))),
                }),
            })),
        ]),
    }
}
