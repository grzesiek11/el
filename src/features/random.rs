/*
 * This file is a part of the El project: <https://gitlab.com/Grzesiek11/El>
 * El is available under the terms of the GNU Affero General Public License, version 3 or later.
 * The license is distributed with the project, but if you don't have a copy, see <https://www.gnu.org/licenses/agpl-3.0.html> instead.
 */

use std::collections::HashMap;

use rand::{thread_rng, Rng, prelude::SliceRandom};

use crate::events::{Context, EventResult, Command, Plugin, CommandMetadata};

async fn random(ctx: Context, cmd: Command) -> EventResult {
    let from = cmd.arguments["from"].value()?.as_i32()?;
    let to = cmd.arguments["to"].value()?.as_i32()?;

    if to < from {
        return Err("`to` can't be less than `from`".into());
    }

    let n = thread_rng().gen_range(from..to + 1);
    cmd.msg.channel_id.say(&ctx.c.http, n).await?;

    Ok(())
}

async fn choice(ctx: Context, cmd: Command) -> EventResult {
    let mut choices = Vec::new();
    for choice in &cmd.arguments["choice"].values {
        choices.push(choice.as_str().unwrap().to_owned());
    }

    let choice = choices.choose(&mut thread_rng()).unwrap();
    cmd.msg.channel_id.say(&ctx.c.http, choice).await?;

    Ok(())
}

async fn coin(ctx: Context, cmd: Command) -> EventResult {
    cmd.msg.channel_id.say(&ctx.c.http, {
        if thread_rng().gen() {
            "Heads"
        } else {
            "Tails"
        }
    }).await?;

    Ok(())
}

pub fn register() -> Plugin {
    Plugin {
        events: Vec::new(),
        commands: HashMap::from([
            ("random".to_string(), cmdparse::Node::Command(cmdparse::Command {
                arguments: HashMap::from([
                    ("from".to_string(), cmdparse::Argument {
                        kind: cmdparse::ArgumentKind::Positional(0),
                        value: cmdparse::Value::I32,
                        default: None,
                        repeating: cmdparse::Repeat::False,
                        required: true,
                    }),
                    ("to".to_string(), cmdparse::Argument {
                        kind: cmdparse::ArgumentKind::Positional(1),
                        value: cmdparse::Value::I32,
                        default: None,
                        repeating: cmdparse::Repeat::False,
                        required: true,
                    }),
                ]),
                subcommands: HashMap::from([
                    ("choice".to_string(), cmdparse::Node::Command(cmdparse::Command {
                        arguments: HashMap::from([
                            ("choice".to_string(), cmdparse::Argument {
                                kind: cmdparse::ArgumentKind::Positional(0),
                                value: cmdparse::Value::String,
                                default: None,
                                repeating: cmdparse::Repeat::True,
                                required: true,
                            }),
                        ]),
                        subcommands: HashMap::new(),
                        parse_negative_numbers: false,
                        metadata: Box::new(CommandMetadata {
                            callback: Some(Box::new(move |context, command| Box::pin(choice(context, command)))),
                        }),
                    })),
                    ("coin".to_string(), cmdparse::Node::Command(cmdparse::Command {
                        arguments: HashMap::new(),
                        subcommands: HashMap::new(),
                        parse_negative_numbers: false,
                        metadata: Box::new(CommandMetadata {
                            callback: Some(Box::new(move |context, command| Box::pin(coin(context, command)))),
                        }),
                    })),
                ]),
                parse_negative_numbers: true,
                metadata: Box::new(CommandMetadata {
                    callback: Some(Box::new(move |context, command| Box::pin(random(context, command)))),
                }),
            })),
        ]),
    }
}
