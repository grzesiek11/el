/*
 * This file is a part of the El project: <https://gitlab.com/Grzesiek11/El>
 * El is available under the terms of the GNU Affero General Public License, version 3 or later.
 * The license is distributed with the project, but if you don't have a copy, see <https://www.gnu.org/licenses/agpl-3.0.html> instead.
 */

use std::collections::HashMap;

use serenity::utils::{ContentSafeOptions, content_safe};

use crate::{
    events::{Context, EventResult, Command, Plugin, CommandMetadata},
    utils::sanitize_markdown,
};

async fn typing(ctx: Context, cmd: Command) -> EventResult {
    const EMOJI: &str = "<a:pisze:876564468086222868>";

    let mut result = format!("{} ", EMOJI);

    let names = &cmd.arguments["text"].values;
    if names.len() > 0 {
        for (i, name) in names.iter().enumerate() {
            let name = sanitize_markdown(&content_safe(&ctx.c.cache, name.as_str()?, &ContentSafeOptions::default(), &[]))
                .replace('\n', "");
            result.push_str(&format!("**{}**", name));

            if i + 2 == names.len() {
                result.push_str(" i ");
            } else if i + 1 != names.len() {
                result.push_str(", ");
            }
        }
    } else {
        result.push_str("Kilka osób");
    }
    result.push(' ');

    if names.len() > 1 {
        result.push_str("piszą");
    } else {
        result.push_str("pisze");
    }
    result.push_str("...");

    if result.len() > 2000 {
        result = format!("{} **A hacker** is typing... *I'm in* 😎", EMOJI);
    }

    cmd.msg.channel_id.say(&ctx.c.http, result).await?;

    Ok(())
}

async fn emotize(ctx: Context, cmd: Command) -> EventResult {
    let special = HashMap::from([
        ('0', "0️⃣"),
        ('1', "1️⃣"),
        ('2', "2️⃣"),
        ('3', "3️⃣"),
        ('4', "4️⃣"),
        ('5', "5️⃣"),
        ('6', "6️⃣"),
        ('7', "7️⃣"),
        ('8', "8️⃣"),
        ('9', "9️⃣"),
        (' ', "⬛"),
        ('#', "#️⃣"),
        ('>', "▶️"),
        ('<', "◀️"),
        ('*', "*️⃣"),
        ('^', "🔼"),
        ('.', "⏺️"),
    ]);
    let mut msg = String::new();
    for ch in cmd.arguments["text"].value()?.as_str()?.chars() {
        if ch.is_ascii_alphabetic() {
            msg.push_str(&format!(":regional_indicator_{}: ", ch.to_lowercase()));
        } else if let Some(special) = special.get(&ch) {
            msg.push_str(special);
            msg.push(' ');
        }
    }
    cmd.msg.channel_id.say(&ctx.c.http, msg).await?;

    Ok(())
}

async fn space(ctx: Context, cmd: Command) -> EventResult {
    let mut msg = String::new();
    for ch in cmd.arguments["text"].value()?.as_str()?.chars() {
        msg.push(ch);
        msg.push(' ');
    }
    cmd.msg.channel_id.say(&ctx.c.http, msg).await?;

    Ok(())
}

async fn say(ctx: Context, cmd: Command) -> EventResult {
    let cleanup_options = ContentSafeOptions::new()
        .clean_channel(false)
        .clean_user(false);

    cmd.msg.channel_id.say(&ctx.c.http, &content_safe(&ctx.c.cache, cmd.arguments["text"].value()?.as_str()?, &cleanup_options, &[])).await?;
    Ok(())
}

pub fn register() -> Plugin {
    Plugin {
        events: Vec::new(),
        commands: HashMap::from([
            ("typing".to_string(), cmdparse::Node::Command(cmdparse::Command {
                arguments: HashMap::from([
                    ("text".to_string(), cmdparse::Argument {
                        kind: cmdparse::ArgumentKind::Positional(0),
                        value: cmdparse::Value::String,
                        default: None,
                        repeating: cmdparse::Repeat::True,
                        required: false,
                    }),
                ]),
                subcommands: HashMap::new(),
                parse_negative_numbers: false,
                metadata: Box::new(CommandMetadata {
                    callback: Some(Box::new(move |context, command| Box::pin(typing(context, command)))),
                }),
            })),
            ("emotize".to_string(), cmdparse::Node::Command(cmdparse::Command {
                arguments: HashMap::from([
                    ("text".to_string(), cmdparse::Argument {
                        kind: cmdparse::ArgumentKind::Positional(0),
                        value: cmdparse::Value::String,
                        default: None,
                        repeating: cmdparse::Repeat::Join,
                        required: true,
                    }),
                ]),
                subcommands: HashMap::new(),
                parse_negative_numbers: false,
                metadata: Box::new(CommandMetadata {
                    callback: Some(Box::new(move |context, command| Box::pin(emotize(context, command)))),
                }),
            })),
            ("space".to_string(), cmdparse::Node::Command(cmdparse::Command {
                arguments: HashMap::from([
                    ("text".to_string(), cmdparse::Argument {
                        kind: cmdparse::ArgumentKind::Positional(0),
                        value: cmdparse::Value::String,
                        default: None,
                        repeating: cmdparse::Repeat::Join,
                        required: true,
                    }),
                ]),
                subcommands: HashMap::new(),
                parse_negative_numbers: false,
                metadata: Box::new(CommandMetadata {
                    callback: Some(Box::new(move |context, command| Box::pin(space(context, command)))),
                }),
            })),
            ("say".to_string(), cmdparse::Node::Command(cmdparse::Command {
                arguments: HashMap::from([
                    ("text".to_string(), cmdparse::Argument {
                        kind: cmdparse::ArgumentKind::Positional(0),
                        value: cmdparse::Value::String,
                        default: None,
                        repeating: cmdparse::Repeat::Join,
                        required: true,
                    }),
                ]),
                subcommands: HashMap::new(),
                parse_negative_numbers: false,
                metadata: Box::new(CommandMetadata {
                    callback: Some(Box::new(move |context, command| Box::pin(say(context, command)))),
                }),
            })),
        ]),
    }
}
